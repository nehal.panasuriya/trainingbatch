'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    //  * Add seed commands here.
    await queryInterface.bulkInsert('Users', [{
      "created_at": "2022-04-12T14:07:18.243Z",
      "id": "dce7bd70-ba69-11ec-9e92-b1ef9b50b9fd",
      "full_name": "Suman Kumar",
      "country_code": 2,
      "updatedAt": "2022-04-12T14:07:18.220Z",
      "createdAt": "2022-04-12T14:07:18.220Z",
      "aadharId": null
    },{
      "created_at": "2022-04-12T14:07:18.243Z",
      "id": "ece7bd70-ba69-11ec-9e92-b1ef9b50b9fd",
      "full_name": "Hiten Kumar",
      "country_code": 3,
      "updatedAt": "2022-04-12T14:07:18.220Z",
      "createdAt": "2022-04-12T14:07:18.220Z",
      "aadharId": null
  }], {});
  },

  async down (queryInterface, Sequelize) {
    //  * Add commands to revert seed here.
     await queryInterface.bulkDelete('Users', null, {});
  }
};
