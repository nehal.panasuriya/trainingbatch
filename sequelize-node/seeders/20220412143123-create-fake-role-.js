'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Roles', [{
       "id":"090c886e-ba6e-11ec-8422-0242ac120002",
      "name":"admin1",
      "updatedAt": "2022-04-12T14:07:18.220Z",
      "createdAt": "2022-04-12T14:07:18.220Z",
    },{
      "id":"154cfcee-ba6e-11ec-8422-0242ac120002",
      "name":"user1",
      "updatedAt": "2022-04-12T14:07:18.220Z",
      "createdAt": "2022-04-12T14:07:18.220Z",
  },{
    "id":"1eabab64-ba6e-11ec-8422-0242ac120002",
    "name":"superuser1",
    "updatedAt": "2022-04-12T14:07:18.220Z",
      "createdAt": "2022-04-12T14:07:18.220Z",
  }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
