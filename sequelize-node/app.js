const  express  = require('express')
var uuid = require('uuid');
var path = require('path');
const {sequelize} = require('./models');
const userRouter = require('./routes/users');
const imagesRouter = require('./routes/images');

const app =  express();
app.use(express.json())
app.use(express.static(path.join(__dirname, 'public')));
app.use('/users',userRouter);
app.use('/images',imagesRouter);

// await amidala.addProfile(queen, { through: { selfGranted: false } });
app.listen({ port: 7000 }, async () => {
  console.log('Server up on htttp://localhost:7000');
  main();
})

async function main(){
  await sequelize.sync({})
  console.log("Database synced!");
}