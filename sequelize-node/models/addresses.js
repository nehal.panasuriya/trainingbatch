'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Addresses extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Addresses.init({
    id: {
      type:DataTypes.UUID,
      default: sequelize.fn('uuid_generate_v4'),
      primaryKey: true},
    name: DataTypes.STRING,
    street: DataTypes.STRING,
    city: DataTypes.STRING,
    country: DataTypes.STRING,
    userId: 
    {
       type:DataTypes.UUID,
       references:{
        model: 'User',
        key:'id'
       }
    },
  }, {
    sequelize,
    tableName: 'Addresses',
    modelName: 'Addresses',
  });
  return Addresses;
};