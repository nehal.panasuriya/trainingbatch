'use strict';
const {
  Model
} = require('sequelize');
const roles = require('./roles');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
    }
    
    // toJSON(){
    //   return {...this.get(),id:undefined}
    // }

  }
  User.init({
    id: {
      type:DataTypes.UUID,
      default: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
    },
    full_name: DataTypes.STRING,
    created_at: {
      type: DataTypes.DATE,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP(3)'),
     },
    country_code: DataTypes.INTEGER,
    aadharId: 
    {
       type:DataTypes.UUID,
       references:{
        model: 'AadharCardDetails',
        key:'id'
       }
    },
  }, {
    sequelize,
    tableName: 'User',
    modelName: 'User'
  });
  return User;
};