'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Images extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Images.init({
    id: {
      type:DataTypes.UUID,
      default: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
    },
    url: DataTypes.STRING,
    height: DataTypes.INTEGER,
    width: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Images',
  });
  return Images;
};