'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Roles extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({User}) {
      // define association here
      this.belongsToMany(User,{
        through:'UserRoles',
        foreignKey:'roleId'
      })
    }
  }
  Roles.init({
    id: {
      type:DataTypes.UUID,
      default: sequelize.fn('uuid_generate_v4'),
      primaryKey: true,
    },
    name: DataTypes.STRING
  }, {
    sequelize,
    tableName:'Roles',
    modelName: 'Roles',
  });
  return Roles;
};