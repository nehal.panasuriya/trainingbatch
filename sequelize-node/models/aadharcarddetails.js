'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class AadharCardDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  AadharCardDetails.init({
    id: 
    {
      type:DataTypes.UUID,
      primaryKey:true,
      default: sequelize.fn('uuid_generate_v4'),
    },
    aadharNumber: DataTypes.STRING,
    name: DataTypes.STRING
  }, {
    sequelize,
    tableName: "AadharCardDetails",
    modelName: 'AadharCardDetails',
  });
  return AadharCardDetails;
};