var express = require('express');
var router = express.Router();
var uuid = require('uuid');
const {sequelize} = require('../models');
/* GET users listing. */

router.post('/',async(req,res)=>{
  const { full_name, country_code} = req.body;
  try{
    let id = uuid.v1(); 
    const user = await sequelize.models.User.create({ id, full_name, country_code });
    return res.json(user);
  }catch(err){
    return res.status(500).json({error: err});
  }
});
router.get('/', async (req, res) => {
  try {
    const users = await  sequelize.models.User.findAll();
    return res.json(users)
  } catch (err) {
    console.log(">>>>> ",err);
    return res.status(500).json({ error: err })
  }
})
router.get('/:id', async (req, res) => {
  const uuid = req.params.id;
  try {
    const user = await sequelize.models.User.findOne({
      where: { id: uuid }
    });
    return res.json(user)
  } catch (err) {
    return res.status(500).json({ error: err })
  }
})
router.put('/:id', async (req, res) => {
    const uuid = req.params.id;
    const { full_name, country_code } = req.body;
    try {
    const user = await sequelize.models.User.findOne({
      where: { id: uuid }
    });
    user.full_name = full_name;
    user.country_code = country_code;
    await user.save();
    return res.json(user)
    } catch (err) {
    return res.status(500).json({ error: err })
  }
})

router.delete('/:id', async (req, res) => {
  const uuid = req.params.id;
  try {
    const user = await sequelize.models.User.findOne({
      where: { id: uuid }
    });
    await user.destroy();
    return res.json(user.name +" deleted successfully!")
  } catch (err) {
    return res.status(500).json({ error: err })
  }
})

router.post('/:id/aadhar',async (req, res) => {
  const userId = req.params.id;
  const { aadharNumber, name} = req.body;
  try {
    let id = uuid.v1();
      const user = await sequelize.models.User.findOne({ where: { id: userId } });
      const aadhar = await sequelize.models.AadharCardDetails.create({id,aadharNumber,name});
      user.aadharId = aadhar.id;
      await user.save()
      return res.json({user,aadhar})
  } catch (err) {
      console.log(err)
      return res.status(500).json({ error: err })
  }
});

router.get('/:id/aadhar',async (req,res) => {
  const userId = req.params.id;
    try{
      const user = await sequelize.models.User.findOne({ where: { id: userId } });
      const aadhar = await sequelize.models.AadharCardDetails.findOne({ where: { id: user.aadharId } });
      res.json(aadhar);
    }catch (err) {
      console.log(err)
      return res.status(500).json({ error: err })
  }
})

router.post('/:id/addresses',async (req,res) => {
  const userId = req.params.id;
  const {name, country, city, street} = req.body;
    try{
      let id = uuid.v1();
      let user = await sequelize.models.User.findOne({ where:{ id: userId } });

      let address = await sequelize.models.Addresses.create({id,name,country,city,street,userId});
      res.json({ user, address });
    }catch (err) {
      console.log(err)
      return res.status(500).json({ error: err })
  }
})

router.get('/:id/addresses',async (req,res) => {
    const userId = req.params.id;
    try{
      let user = await sequelize.models.User.findOne({ where:{ id: userId } });
      let address = await sequelize.models.Addresses.findAll({ where: { userId: userId } });
      res.json({ user, address });
    }catch (err) {
      console.log(err)
      return res.status(500).json({ error: err })
  }
})

router.get('/:id/addresses/:add_id',async (req,res) => {
  const userId = req.params.id;
  const addressId = req.params.add_id;
  try{
    let address = await sequelize.models.Addresses.findOne({ where: { id:addressId,userId: userId } });
    res.json(address);
  }catch (err) {
    console.log(err)
    return res.status(500).json({ error: err })
}
})

router.put('/:id/addresses/:add_id',async (req,res) => {
  const userId = req.params.id;
  const addressId = req.params.add_id;
  const {name, country, street, city} = req.body;
  try{
    let address = await sequelize.models.Addresses.findOne({ where: { id: addressId, userId: userId } });
    address.name = name;
    address.country = country;
    address.street = street;
    address.city = city; 
    await address.save();
    return res.json(address);
  } catch(err){
    console.log(err)
    return res.status(500).json({ error: err })
  }
})
router.post('/:id/roles',async (req,res) => {
  const userId = req.params.id;
  const { name } = req.body;
    try{
      let id = uuid.v1();
      let user = await sequelize.models.User.findOne({ where:{ id: userId } });
      let roles = await sequelize.models.Roles.create({ id: id, name: name });
      let userRoles = await user.addRoles(roles, { through: { selfGranted: false } });

      res.json({ user, roles, userRoles });
    }catch (err) {
      console.log(err)
      return res.status(500).json({ error: err })
  }
})
router.get('/:id/roles',async (req,res) => {
  const userId = req.params.id;
  const { name } = req.body;
    try{
      let id = uuid.v1();
      let user = await sequelize.models.User.findAll({ where:{ id: userId } });
      // let userroles = await sequelize.models.UserRoles.findOne({ where:{ userId: userId } });
      // let roles = await sequelize.models.Roles.findOne({ where:{ id: userroles.roleId } });

      res.json({ user });
    }catch (err) {
      console.log(err)
      return res.status(500).json({ error: err })
  }
})

module.exports = router;
