var express = require('express');
var router = express.Router();
var uuid = require('uuid');
const multer = require('multer');
const upload = multer({ dest: 'public/images' })
const { sequelize } = require('../models');

router.post('/', upload.single('userImage'), (async (req, res) => {
    let file = req.file.path;
    let path = file.substring(file.indexOf('/'));
    let url = "http://localhost:5000" + path;
    let width = 550;
    let height = 550;

    try {
        let id = uuid.v1(); // -> '6c84fb90-12c4-11e1-840d-7b25c5ee775a'
        const image = await sequelize.models.Images.create({ id, url, width, height });
        return res.json(image);
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}));
router.post('/:id/comments', (async (req, res) => {
    let commentableType = 'image';
    let commentableId = req.params.id;
    let text = req.body.text;
    try {
        let id = uuid.v1();
        const comment = await sequelize.models.Comments.create({ id, text, commentableType, commentableId });
        return res.json(comment);
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}));

router.get('/:id/comments', (async (req, res) => {
    let imageId = req.params.id;
    try {
        const comments = await sequelize.models.Comments.findAll({
             where: { 
                 commentableId: imageId 
                } ,
                include:sequelize.models.Images
            });
        return res.json(comments);
    } catch (err) {
        return res.status(500).json({ error: err });
    }
}))





module.exports = router;