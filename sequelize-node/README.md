Setup in your system
--------
Setup Nodejs + express
Install postgresql
Install sequelize


After installing sequelize on you system run these on terminal
--------------
sequelize init 
sequelize db:create   
// Create User to User
sequelize model:generate --name User --attributes id:uuid,full_name:string,created_at:DATE,country_code:INTEGER,aadharId:uuid

// Create User to AadharCardDetails
sequelize model:generate --name AadharCardDetails --attributes id:uuid,aadharNumber:string,name:string


Seeder:
sequelize seed:generate --name create-fake-users
sequelize seed:generate --name create-fake-role

To seed data on tables:
sequelize db:seed:all

Create a migration file:

Run A Migration File
sequelize db:migrate 20220412141857-migration-skeleton.js