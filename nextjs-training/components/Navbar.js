import navStyles from "../styles/Nav.module.css";
// import headerStyles from  "../";
import Link from 'next/link';

const Navbar = () => {
    return (
        <nav className={navStyles.nav}>
            <ul>
                <li>
                    <Link href='/'>Home</Link>
                </li>&nbsp;&nbsp;&nbsp;
                 <li>
                    <Link href='/about'>About</Link>
                </li>
                {/* <li>
                    <Link href='/'>About</Link>
                </li> */}
            </ul>
        </nav>
    )
}
export default Navbar;