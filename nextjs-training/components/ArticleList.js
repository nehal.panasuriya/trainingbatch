import ArticleItem from "./ArticleItem";
import articleStyles from '../styles/Articals.module.css';

const ArticleList = ({articles}) => {
    console.log("articles111",articles)
    return (
        <div className={articleStyles.grid}>
            {articles.map((article,index) => (
                    <ArticleItem article={article} key={index} />
            ))}
        </div>
        
    )
}
export default ArticleList;