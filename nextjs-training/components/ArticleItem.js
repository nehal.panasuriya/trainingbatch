import Link from "next/link"
import articalStyles from '../styles/Articals.module.css'

const ArticleItem = ({article}) => {
    return(
        <Link href="/article/[id]" as={`/article/${article.id}`}>
            <a className={articalStyles.card}>
                <h3>{article.title} &rarr;</h3>
                <p> {article.body}</p>
            </a>
        </Link>
    )
}
export default ArticleItem