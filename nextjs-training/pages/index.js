import {server} from '../config';
import Head from 'next/head'
import Image from 'next/image'
import ArticleList from '../components/ArticleList'

export default function Home({ articles }) {
  return (
    <div>
      <Head>
        <title>WebDev News</title>
        <meta name='keywords' content='Web developement Programming' />
      </Head>
      <h1>
        Welcome To Next
      </h1>
      <ArticleList articles={articles}/>
    </div>  
  )
}
export const getStaticProps = async () => {
  const res = await fetch(`http://localhost:3002/api/articles`)
  const articles = await res.json()
  
  return {
    props: {
      articles
    } 
  }
}
