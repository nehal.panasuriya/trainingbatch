let todos = [];

export const getTodos = () => {
    return todos;
}

export const addTodo = (todo) => {
    todo.id = Date.now();
    todos.push(todo);

}

export const getTodoById = (id) => {
    let todo = todos.filter((td) => td.id == id);
    return todo[0];
}

