let users = [];

console.log("users",users)
export const getUsers = () => {
    return users;
}
export const addUser = (user) => {
    user.id = Date.now();
    users.push(user);
}
export const addUserData = (user) => {
    user.id = Date.now();
    users.push(user);

}
export const getUserById = (userId) => {
    let user = users.filter((usr) => usr.id == userId);
    return user[0];
}
export const login = (mobile,password) => {
    let user = users.filter((usr) => usr.mobile == mobile && usr.password == password);
    return user[0];
}