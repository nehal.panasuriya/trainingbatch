import { useForm } from 'react-hook-form';
import { useState, useEffect } from 'react';

export default function ToDoInputSection({ btn, data, addTodoData }) {
  const { register, handleSubmit, reset } = useForm();
  const onHandleSubmit = (todo) => {
    reset({name:"",description:"",time:""});
    addTodoData(todo);
  }

  useEffect(() => {
    reset(data);
  },[btn,data]);

  return (
    <form className="p-10 w-1/3 m-auto" onSubmit={handleSubmit(onHandleSubmit)}>
      <div className="flex flex-col m-1">
        <div className="flex flex-row justify-between flex-wrap m-2">
          <label>Name : </label>
          <input type="text" className="border-2 border-black"
            {...register("name")} />
        </div>
        <div className="flex flex-row justify-between flex-wrap m-2">
          <label>Description : </label>
          <input type="text" className="border-2 border-black"  {...register("description")} />
        </div>
        <div className="flex flex-row justify-between flex-wrap m-2">
          <label>Time (in hours) : </label>
          <input type="number" className="border-2 border-black"  {...register("time")} />
        </div>
        <input className="border-2 border-black p-2 block m-auto" type="submit"
          value={btn}
        />
      </div>
    </form>
  )
}

