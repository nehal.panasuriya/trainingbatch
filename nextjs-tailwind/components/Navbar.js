import { Fragment } from 'react'
// import { Disclosure, Menu, Fragment } from '@headlessui/react'
// import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline'
import { useRouter } from 'next/router'


function classNameNames(...classNamees) {
  return classNamees.filter(Boolean).join(' ')
}

export default function Navbar() {
  const router = useRouter();

  return (
    
<nav className="bg-gray-800">
  <div className="max-w-7xl mx-auto px-2 sm:px-6 lg:px-8">
    <div className="relative flex items-center justify-between h-16">
      <div className="flex-1 flex items-center justify-center sm:items-stretch sm:justify-start">
        <div className="flex-shrink-0 flex items-center">
            <p className="text-white">Todo</p>
        </div>
        <div className="hidden sm:block sm:ml-6">
          <div className="flex space-x-4"> 
            <button  className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium" aria-current="page" onClick={() => router.push('/login')}>Login</button>
            <button className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium" onClick={() => router.push('/register')}>Register</button>
          </div>
        </div>
      </div>
      
    </div>
  </div>

  
  <div className="sm:hidden" id="mobile-menu">
    <div className="px-2 pt-2 pb-3 space-y-1">
      <button  className="bg-gray-900 text-white block px-3 py-2 rounded-md text-base font-medium" aria-current="page" onClick={() => router.push('/login')}>Login</button>
      <button  className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium" onClick={() => router.push('/register')}>Register</button>
    </div>
  </div>
</nav>

  )
}
