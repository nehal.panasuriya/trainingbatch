import { useState, useEffect } from "react"
import Navbar from '../components/Navbar';
import ToDoInputSection from '../components/ToDoInputSection';

export default function Dashboard() {
    const [todos, setTodos] = useState([]);
    const [btn, setBtn] = useState('Add');
    const [data, setData] = useState();
    const [index, setIndex] = useState();
    const [current, setCurrent] = useState('in progress')

    const addTodoData = (data) => {
        if (btn == 'Add') {
            console.log("datadata", data)
            setTodos([...todos, { ...data, status: "in progress" }]);
        }else {
            let todo = todos.filter((data) => data.id == index)
            todo[0].name = data.name;
            todo[0].description = data.description;
            todo[0].time = data.time;
            setData();
            setTodos([...todos]);
            setBtn('Add');
        }
    }
    const onComplete = (index) => {
        const newTodo = [...todos];
        newTodo[index]["status"] = "completed";
        setTodos(newTodo);
    }
    const onEdit = (item, idx) => {
        console.log(item);
        setBtn('update');
        setData(item);
        setIndex(item.id)
    }

    return (
        <>
            <Navbar />
            <h1 className="w-full text-center mt-5">Welcome to Todos !!</h1>
            <ToDoInputSection btn={btn} data={data} addTodoData={addTodoData} />
            <div className="w-1/2 m-auto flex flex-row ">
                <button className={current == 'in progress' ? "w-1/2 bg-gray-700 text-white" : "w-1/2 hover:bg-gray-700 hover:text-white"} onClick={() => { setCurrent('in progress') }}>In Progress</button>
                <button className={current == 'completed' ? "w-1/2 bg-gray-700 text-white" : "w-1/2 hover:bg-gray-700 hover:text-white"} onClick={() => { setCurrent('completed') }}>Completed</button>
            </div>
            <div className="p-10 w-2/3 m-auto">
                <h2 className="text-center mb-5">Todos</h2>
                {
                    (current === 'in progress') &&
                    (todos.map((item, index) => {
                       item.id = index + 1;
                        if (item.status == 'in progress') {
                            return (
                                <div className="w-1/2 m-auto flex flex-row justify-between mb-5" key={index}>
                                    <button onClick={() => {
                                        onEdit(item);
                                    }}>Edit</button>
                                    <p>{item.id}</p>
                                    <p>{item.name}</p>
                                    <p>{item.description}</p>
                                    <p>{item.time}</p>
                                    <button className="border-2 border-black p-1" onClick={() => onComplete(index)}>Complete</button>
                                </div>
                            )
                        }
                    }))}
                {
                    (current === 'completed') &&
                    (todos.map((item, index) => {
                        if (item.status == 'completed') {
                            return (
                                <div className="w-1/2 m-auto flex flex-row justify-between mb-5">
                                    <p>{index + 1}</p>
                                    <p>{item.name}</p>
                                    <p>{item.description}</p>
                                    <p>{item.time}</p>
                                </div>
                            )
                        }
                    }))}
            </div>
        </>
    )
}