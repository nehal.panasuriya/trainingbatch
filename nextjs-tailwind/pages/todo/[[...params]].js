import { useState, useEffect } from "react"
import { useForm } from 'react-hook-form';
import Navbar from "../../components/Navbar";
import { Router, useRouter } from "next/router";
import { getTodoById,addTodo } from "../../Service/Todo";

export default function TodoForm() {
    const router = useRouter()
    const [todo, setTodo] = useState({});
    const [btn,setBtn] = useState('Add')
    const { register, resetField, handleSubmit,defaultValues } = useForm();

    const onSubmit = (data) => {
        if(btn == 'Add')
        {
            console.log('data',data)
            addTodo({...data,status : "in progress"});
            router.push('/dashboard');
        }
        else
        {
            // updateTodo({...data,})
        }
        resetField('todo');
        resetField('description');
        resetField('time');
    }
    

    useEffect(() => {
        if(router.query.params)
        {
            let tdData = getTodoById(router.query.params[0]);
            setBtn('update');
            setTodo(getTodoById(router.query.params[0]));
            console.log("tdData",tdData);
        }
        
    },[]);
    return (
        <>
            <Navbar/>
            <form className="p-10 w-1/3 m-auto" onSubmit={handleSubmit(onSubmit)}>
                <div className="flex flex-col m-1">
                    <div className="flex flex-row justify-between flex-wrap m-2">
                    <label>Name : </label>
                    <input type="text" className="border-2 border-black" {...register("todo")} />
                    </div>
                    <div className="flex flex-row justify-between flex-wrap m-2">
                    <label>Description : </label>
                    <input type="text" className="border-2 border-black" {...register("description")} />
                    </div>
                    <div className="flex flex-row justify-between flex-wrap m-2">
                    <label>Time (in hours) : </label>
                    <input type="number" className="border-2 border-black" {...register("time")} />
                    </div>
                    
                    <input className="border-2 border-black p-2 block m-auto" type="submit" value={btn} />
                </div>
            </form>
        </>
    )
}