import {useForm} from 'react-hook-form';
import { login } from '../Service/StaticData';
import { useRouter } from 'next/router';
import Navbar from '../components/Navbar';

export default function Login() {
  const router = useRouter();
  const { register, handleSubmit} = useForm();
    // Submit your data into Redux store
    const onSubmit = (data) => {
        let user = login(data.mobile,data.password);
        if(user)
        {
          console.log("User",user);
          router.push('/dashboard');

        }
    };


  return (
    <>
    <Navbar/>
    <form className="p-10 w-1/3 m-auto" onSubmit={handleSubmit(onSubmit)}>
            <div className="flex flex-row justify-evenly m-5">
                <label>Mobile</label>
                <input type = "number" className="border-2 border-black" {...register("mobile", { required: true })} />
            </div>
            <div className="flex flex-row justify-evenly m-5">
                <label>Password</label>
                <input type="password" className="border-2 border-black" {...register("password", {})} />
            </div>
            <input className = "border-2 border-black p-2 block m-auto" type="submit" value="login" />
        </form>
        </>
  )
}
