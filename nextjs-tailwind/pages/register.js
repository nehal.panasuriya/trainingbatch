import {useForm} from 'react-hook-form';
import { addUser } from '../Service/StaticData';
import {useRouter} from 'next/router';
import Navbar from '../components/Navbar';

export default function Register() {
    const router = useRouter()
    const { register, handleSubmit} = useForm();
    // Submit your data into Redux store
    const onSubmit = (data) => {
        addUser(data);
        router.push('/login');
    };

    return (
        <>
        <Navbar/>
        <form className="p-10 w-1/3 m-auto" onSubmit={handleSubmit(onSubmit)}>
            <div className="flex flex-row justify-evenly m-5">
                <label>Name</label>
                <input type = "text" className="border-2 border-black" {...register("name")} />
            </div>
            <div className="flex flex-row justify-evenly m-5">
                <label>Mobile</label>
                <input type = "number" className="border-2 border-black" {...register("mobile", { required: true })} />
            </div>
            <div className="flex flex-row justify-evenly m-5">
                <label>Gender</label>
                <div>
                    
                <input {...register("gender")} type="radio" value="Male" />
                <label className='mr-5'>Male</label>
                <input {...register("gender")} type="radio" value="Female" />  
                <label>Female</label>
                </div>
            </div>
            <div className="flex flex-row justify-evenly m-5">
                <label>Email</label>
                <input type="text" className="border-2 border-black"  {...register("Email", {required: true, pattern: /^\S+@\S+$/i})} />

            </div>
            <div className="flex flex-row justify-evenly m-5">
                <label>Password</label>
                <input type="password" className="border-2 border-black" {...register("password", {})} />
            </div>

            
            <input className = "border-2 border-black p-2 block m-auto" type="submit" />

        </form>
        </>
    );
}
  