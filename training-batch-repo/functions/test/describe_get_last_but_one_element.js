// const assert = require('chai').assert;
const expect =require('chai').expect;
const app = require('../app.js')

describe("find last but one element in list",()=>{
    context('when the list empty',()=>{
        it('it should return nothing',() => {
            let result = findLastButElement([]);
            expect(result).to.be.undefined
        })
    })
    context('when the list containt one element',()=>{
        it('it should return first element',() => {
            let result = findLastButElement([1,2]);
            expect(result).to.be.eql(1)
        })
    })
    context('when the list containt two element',()=>{
        it('it should return first element',() => {
            let result = findLastButElement([1,2]);
            expect(result).to.be.eql(1)
        })
    })
    context('when the list containt many Element',()=>{
        it('it should last Element',() => {
            let result = findLastButElement([1,2,3]);
            expect(result).to.be.eql(2)
        })
    })
})
const findLastButElement = (list) =>{
    return list[list.length-2]
}
