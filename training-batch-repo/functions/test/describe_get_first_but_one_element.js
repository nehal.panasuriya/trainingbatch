const assert = require('chai').assert;
const expect =require('chai').expect;
// const app = require('../app.js')

//use .only then get output for only one function
describe("find last element in list",()=>{
    context('when the list empty',()=>{
        it('it should return nothing',() => {
            let result = findFirstButElement([]);
            expect(result).to.be.undefined
        })
    })
    context('when the list containt one element',()=>{
        it('it should return first element',() => {
            let result = findFirstButElement([100]);
            expect(result).to.be.eql(100)
        })
    })
    context('when the list containt many Element',()=>{
        it('it should last Element',() => {
            let result = findFirstButElement([100,101]);
            expect(result).to.be.eql(101)
        })
    })
})
const findFirstButElement = (list) =>{
    return list[list.length-1]
}
