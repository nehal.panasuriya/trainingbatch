var assert = require('assert');
const expect = require('chai').expect;
// const app = require('../app.js')

describe.only('Haskell', () => {
    context('Problem 1', () => {
        it('it should return findTheLastElement of list[1, 2, 3, 4, 5, 6, 7]', () => {
            let result = findTheLastElement([1, 2, 3, 4, 5, 6, 7]);
            expect(result).to.be.eql(7)
        })
        it('it should return findTheLastElement of list[a, b, c, d, e, f]', () => {
            let result = findTheLastElement(['a', 'b', 'c', 'd', 'e', 'f']);
            expect(result).to.be.eql('f')
        })
    })
    //
    context('Problem 2', () => {
        it('it should return last but one element of a list[1, 2, 3, 4]', () => {
            let result = findTheLastButOneElement([1, 2, 3, 4]);
            expect(result).to.be.eql(3)
        })
        it('it should return findTheLastElement of list[x, y, z]', () => {
            let result = findTheLastButOneElement(['x', 'y', 'z']);
            expect(result).to.be.eql('y')
        })
    })
    context('Problem 4', () => {
        it('it should return number of elements of a list', () => {
            let result = findNumberOfElement([123, 456, 789]);
            expect(result).to.be.eql(3)
        })
        it('it should return number of elements of a list[Hello, world!]', () => {
            let result = findNumberOfElement("Hello, world!");
            expect(result).to.be.eql(13)
        })
    })
    context('Problem 5', () => {
        it('it should return Reverse a list', () => {
            let result = reverceList([1, 2, 3, 4]);
            expect(result).to.be.eql([4, 3, 2, 1])
        })
        it('it should return Reverse a list', () => {
            let result = reverceString("A man, a plan, a canal, panama!");
            expect(result).to.be.eql("!amanap ,lanac a ,nalp a ,nam A")
        })
    })
    context('Problem 6 whether a list is a palindrome.', () => {
        it('it should return palindrome of a list', () => {
            let result = isPalindrome([1, 2, 3]);
            expect(result).to.be.eql(false)
        })
        it('it should return palindrome of a string', () => {
            let result = isPalindrome("madamimadam");
            expect(result).to.be.eql(true)
        })
    })
    context('Problem 7 ', () => {   
        it('it should return palindrome of a [1,2,4,8,16,8,4,2,1]', () => {
            let result = isPalindrome([1, 2, 4, 8, 16, 8, 4, 2, 1]);
            expect(result).to.be.eql(true)
        })
    })
    context('Problem 8 Eliminate consecutive duplicates of list elements', () => {
        it('it should return Eliminate consecutive duplicates of list elements', () => {
            let result = compressString(['aaaabccaadeeee']);
            expect(result)
        })
    })
    context('problemm 9 Pack consecutive duplicates of list elements into sublists', () => {
        it('it should return Pack consecutive duplicates of list elements into sublists', () => {
            let result = compressString(['aaaabccaadeeee']);
            expect(result)
        })
    })
    context('problem 10 Run-length encoding of a list', () => {
        it('it should return Run-length encoding of a list', () => {
            let result = encodeList('wwwwaaadexxxxxxywww')
            expect(result)
        })
    })
});
const findTheLastElement = (list) => {
    return list[list.length - 1];
}
const findTheLastButOneElement = (list) => {
    return list[list.length - 2];
}
const findNumberOfElement = (list) => {
    const number = list.length;
    return number
}
const reverceList = (list) => {
    const listReverse = list.reverse()
    return listReverse
}
const reverceString = (list) => {
    list = [...list].reverse().join("");
    return list
}
const isPalindrome = (list) => {
    const len = list.length;
    for (let i = 0; i < len / 2; i++) {
        if (list[i] !== list[len - 1 - i]) {
            return false;
        }
    }
    return true;
}
const compressString = (list) => {
    let n = list.length;
    let str = "";
    if (n == 0)
        return str;
    for (let i = 0; i < n - 1; i++) {
        if (list[i] != list[i + 1]) {
            str += list[i];
        }
    }
    str += list[n - 1];
    return str;
};
const flatten = (arr) => {
    const flatNumber = arr.flat()
    return flatNumber
}
const encodeList = (str) => {
    let n = str.length;
    for (let i = 0; i < n; i++) {
        let count = 1;
        while (i < n - 1 && str[i] == str[i + 1]) {
            count++;
            i++;
        }
       return count,str[i]
    }
}
