var express = require('express');
const app = express()
app.use(express.json())

const tasks = [
  {
    id: 1,
    name: "Task 1",
    completed: true
  },
  {
    id: 2,
    name: "Task 2",
    completed: false
  },
  {
    id: 3,
    name: "Task 3",
    completed: true
  },
]
/* GET users listing. */
app.get('/api/tasks', (req, res, next) => {
  res.send(tasks);
});

app.get('/api/tasks/:id', (req, res, next) => {
  const taskId = req.params.id;
  const task = tasks.find(task => task.id === parseInt(taskId))
  if(!task) return res.status(404).send("Id not found") 
  res.send(task);
});

app.post('/api/tasks/',(req, res, next)=>{
  const task = {
    id:tasks.length+1,
    name:req.body.name,
    completed:req.body.completed
  }
  tasks.push(task)
  if(!task) return res.status(404).send("Id not found") 
  res.status(200).send(task)  
})

app.put('/api/tasks/:id',(req,res) => {
  const taskId = req.params.id;
  const task = tasks.find(task => task.id === parseInt(taskId))
  task.name=req.body.name
  task.completed=req.body.completed
  res.send(task)
})
app.delete('/api/tasks/:id',(req,res) => {
  const taskId = req.params.id;
  const task = tasks.find(task => task.id === parseInt(taskId))
  if(!task) return res.status(400).send("This Id Record not found")
  const index = tasks.indexOf(task);
  tasks.splice(index,1)
  res.send(task)
});
const port = process.env.PORT || 9000;
module.exports = app.listen(port,()=>console.log(`listing on port ${port}....`));
