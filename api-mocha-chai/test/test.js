let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../index')
// let router = require('../routes/users')
chai.should();
chai.use(chaiHttp);

describe('Task Api', () => {
    describe("GET api/tasks", () => {
        it("it should Get all Record ", (done) => {
            chai.request(server)
                .get("/api/tasks")
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eq(3)
                    done();
                });
        })
        it("it should Not Get all Record ", (done) => {
            chai.request(server)
                .get("/api/task")
                .end((err, res) => {
                    res.should.have.status(404);
                    done();
                });
        })
    })
    // Test the Get(Id) Route
    describe("GET api/tasks/:id", () => {
        it("it should Get Record by ID", (done) => {
            const taskId = 1;
            chai.request(server)
                .get("/api/tasks/" + taskId)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id')
                    res.body.should.have.property('name')
                    res.body.should.have.property('completed')
                    res.body.should.have.property('id').eq(1)
                    done();
                });
        })
        it("it should Not Get Record by ID", (done) => {
            const taskId = 123;
            chai.request(server)
                .get("/api/tasks/" + taskId)
                .end((err, res) => {
                    res.should.have.status(404);
                    res.text.should.be.eql("Id not found");
                    done();
                });
        })
    })
    // Test the Post Route
    describe("POST api/tasks/", () => {
        it("it should Post New Record", (done) => {
            const task = {
                name: "Taskd 5",
                completed: true
            };
            chai.request(server)
                .post("/api/tasks")
                .send(task)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id').eql(4)
                    res.body.should.have.property('name').eql('Taskd 5')
                    res.body.should.have.property('completed').eql(true)
                    // res.body.should.have.property('completed').eq(true)
                    done();
                });
        })
    })
    // Test the Put Route
    describe("PUT /api/tasks/:id", () => {
        it("it should PUT New Record", (done) => {
            const taskId = 1;
            const task = {
                name: "Taskd 5",
                completed: true
            };
            chai.request(server)
                .put("/api/tasks/" + taskId)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('id').eql(1)
                    res.body.should.have.property('name').eql('Taskd 5')
                    res.body.should.have.property('completed').eql(true)
                    // res.body.should.have.property('completed').eq(true)
                    done();
                });
        })
    })
    // Test the Delete Route
    describe("Delete /api/tasks/:id", () => {
        it("it should DELETE  Record", (done) => {
            const taskId = 1;
            const task = {
                name: "Taskd 5",
                completed: true
            };
            chai.request(server)
                .delete("/api/tasks/" + taskId)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        })
        it("it should Not DELETE Record", (done) => {
            const taskId = 111;
            const task = {
                name: "Taskd 5",
                completed: true
            };
            chai.request(server)
                .delete("/api/tasks/" + taskId)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.eql('This Id Record not found');
                    done();
                });
        })
    })
})