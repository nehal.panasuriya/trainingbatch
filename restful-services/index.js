const { use } = require('chai');
var express = require('express');
const app = express()
app.use(express.json())

const users = [
  {
    id: 1,
    name: "user 1",
    email: "user1@gmail.com",
    aadhar: 2121212121221,
    addresses:"Gujarat India"
  },
  {
    id: 2,
    name: "user 2",
    email: "user2@gmail.com",
    aadhar: 214456721221,
    addresses:"Gujarat India"
  },
  {
    id: 3,
    name: "user 3",
    email: "user3@gmail.com",
    aadhar: 135644521221,
    addresses:"hydrabad India"
  },
]
/* GET users listing. */
app.get('/api/users', (req, res, next) => {
  res.send(users);
});
app.post('/api/users/', (req, res, next) => {
  const user = {
    id: users.length + 1,
    name: req.body.name,
    email: req.body.email
  }
  users.push(user)
  res.status(200).send(user)
})
app.get('/api/users/:id', (req, res, next) => {
  const userId = req.params.id;
  const user = users.find(user => user.id === parseInt(userId))
  if (!user) return res.status(404).send("Id not found")
  res.send(user);
});
app.put('/api/users/:id', (req, res) => {
  const userId = req.params.id;
  const user = users.find(user => user.id === parseInt(userId))
  user.name = req.body.name
  user.email = req.body.email
  res.send(user)
})
app.delete('/api/users/:id', (req, res) => {
  const userId = req.params.id;
  const user = users.find(user => user.id === parseInt(userId))
  if (!user) return res.status(400).send("This Id Record not found")
  const index = users.indexOf(user);
  users.splice(index, 1)
  res.send(user)  
});
app.post('/api/users/:id/:aadhar', (req, res, next) => {
  const userId = req.params.id;
  const usr = users.find(usr => usr.id === parseInt(userId))
  const aadharNum = req.params.aadhar;
  console.log("aadharNum", aadharNum)
  console.log("usr", usr)
  // if(aadharNum) return res.status(400).send("AadharNumber is already added")
  const userData = {
    id: usr.id,
    name: usr.name,
    email: usr.email,
    aadhar: aadharNum,
  }
  console.log("userData",userData)
  users.push(userData)
  res.status(200).send(userData)  
})
app.post('/api/users/:id/addresses', (req, res, next) => {
  const userId = req.params.id;
  const usr = users.find(usr => usr.id === parseInt(userId))
  const userData = {
    id: usr.id,
    name: usr.name,
    email: usr.email,
    aadhar:usr.aadhar,
    addresses:req.body.addresses
  }
  console.log("userData",userData)
  users.push(userData)
  res.status(200).send(userData)  
})
app.get('/api/users/:id', (req, res, next) => {
  const userId = req.params.id;
  const user = users.find(user => user.id === parseInt(userId))
  if (!user) return res.status(404).send("Id not found")
  res.send(user);
});
// get  will retrieve a specific address for a user
app.get('/api/users/:id/addresses/:id', (req, res, next) => {
  const userId = req.params.id;
  const user = users.find(user => user.id === parseInt(userId))
  if (!user) return res.status(404).send("Id not found")
  res.send(user.addresses);
});

// - put will update the address of a user
app.put('/api/users/:id/addresses/:id', (req, res) => {
  const userId = req.params.id;
  const user = users.find(user => user.id === parseInt(userId))
  user.addresses =req.body.addresses
  res.send(user)
})

const port = process.env.PORT || 7000;
module.exports = app.listen(port, () => console.log(`listing on port ${port}..`));